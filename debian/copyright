Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kdeplasma-addons
Source: https://invent.kde.org/plasma/kdeplasma-addons
Upstream-Contact: plasma-devel@kde.org

Files: *
Copyright: 2013, Aaron Seigo <aseigo@kde.org>
           2013-2015, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2018, Aleix Pol Gonzalez <aleixpol@kde.org>
           2020-2022, Alexander Lohnau <alexander.lohnau@gmx.de>
           2008, Anne-Marie Mahfouf <annma@kde.org>
           2015-2017, Bernhard Friedreich <friesoft@gmail.com>
           2013, Bhushan Shah <bhush94@gmail.com>
           2020, Chris Holland <zrenfire@gmail.com>
           2012-2016, David Edmundson <davidedmundson@kde.org>
           2017, David Faure <faure@kde.org>
           2007-2014, Davide Bettio <davide.bettio@kdemail.net>
           2016-2018, Friedrich W. H. Kossebau <kossebau@kde.org>
           2022, Fushan Wen <qydwhotmail@gmail.com>
           2021, Gary Wang <wzc782970009@gmail.com>
           2008, Georges Toth <gtoth@trypill.org>
           2019-2021, Guo Yunhe <i@guoyunhe.me>
           2013, Heena Mahour <heena393@gmail.com>
           2007, Henry Stanaland <stanaland@gmail.com>
           2020-2021, Ismael Asensio <isma.af@gmail.com>
           2022, ivan (@ratijas) tkachenko <me@ratijas.tk>
           2010-2012, Jason A. Donenfeld <Jason@zx2c4.com>
           2014, Jeremy Whiting <jpwhiting@kde.org>
           2014, Joseph Wenninger <jowenn@kde.org>
           2014-2018, Kai Uwe Broulik <kde@privat.broulik.de>
           2015, Lars Pontoppidan <dev.larpon@gmail.com>
           2008, Laurent Montel <montel@kde.org>
           2012, Luiz Romário Santana Rios <luizromario@gmail.com>
           2012, Luís Gabriel Lima <lampih@gmail.com>
           2013-2015, Marco Martin <mart@kde.org>
           2008, Marco Martin <notmart@gmail.com>
           2011-2013, Martin Gräßlin <mgraesslin@kde.org>
           2013, Martin Klapetek <mklapetek@kde.org>
           2014, Martin Yrjölä <martin.yrjola@gmail.com>
           2008-2012, Matthias Fuchs <mat69@gmx.net>
           2014-2016, Mikhail Ivchenko <ematirov@gmail.com>
           2008, Montel Laurent <montel@kde.org>
           2007-2009, Petri Damstén <damu@iki.fi>
           2018, Piotr Kąkol <piotrkakol@protonmail.com>
           2012, Reza Fatahilah Shah <rshah0385@kireihana.com>
           2007, Riccardo Iaconelli <riccardo@kde.org>
           2008, Ryan P. Bitanga <ryan.bitanga@gmail.com>
           2013, Sebastian Kügler <sebas@kde.org>
           2011-2012, Shaun Reich <shaun.reich@kdemail.net>
           2007-2009, Shawn Starr <shawn.starr@rogers.com>
           2020, Sora Steenvoort <sora@dillbox.me>
           2007, Tobias Koenig <tokoe@kde.org>
           2019, Vlad Zahorodnii <vlad.zahorodnii@kde.org>
           2016-2017, Weng Xuetian <wengxt@gmail.com>
           2020, Łukasz Korbel <corebell.it@gmail.com>
           2017-2018, Free Software Foundation
           2007-2017, Free Software Foundation, Inc
           2010-2012, Jason A. Donenfeld <Jason@zx2c4.com>
           2009, K Desktop Environment
           2008-2014, Rosetta Contributors and Canonical Ltd
           2009-2018, The Free Software Foundation
           2009-2018, The Free Software Foundation, Inc
           2012-2014, This file is distributed under the same license as the bosnianuniversetranslation package
           2008-2010, This file is distributed under the same license as the kdeplasma-addons package
           2007-2018, This_file_is_part_of_KDE
License: GPL-2.0-or-later

Files: applets/comic/engine/CMakeLists.txt
       applets/notes/plugin/documenthandler.cpp
       applets/notes/plugin/documenthandler.h
       runners/spellchecker/autotests/CMakeLists.txt
Copyright: 2022, Alexander Lohnau <alexander.lohnau@gmx.de>
           2013, Digia Plc and /or its subsidiary<-ies> <http://www.qt-project.org/legal>
           2022, Fushan Wen <qydwhotmail@gmail.com>
License: BSD-3-Clause

Files: applets/colorpicker/package/contents/config/config.qml
       applets/colorpicker/package/contents/ui/configGeneral.qml
       applets/colorpicker/plugin/colorpickerplugin.cpp
       applets/colorpicker/plugin/colorpickerplugin.h
       applets/colorpicker/plugin/grabwidget.cpp
       applets/colorpicker/plugin/grabwidget.h
       applets/fifteenPuzzle/package/contents/config/config.qml
       applets/fifteenPuzzle/package/contents/ui/configAppearance.qml
       applets/fifteenPuzzle/plugin/fifteenimageprovider.cpp
       applets/fifteenPuzzle/plugin/fifteenimageprovider.h
       applets/fifteenPuzzle/plugin/fifteenpuzzleplugin.cpp
       applets/fifteenPuzzle/plugin/fifteenpuzzleplugin.h
       applets/fuzzy-clock/package/contents/config/config.qml
       applets/fuzzy-clock/package/contents/ui/configAppearance.qml
       applets/notes/package/contents/config/config.qml
       applets/notes/package/contents/ui/configAppearance.qml
       applets/quicklaunch/package/contents/config/config.qml
       applets/quicklaunch/package/contents/ui/ConfigGeneral.qml
       applets/quicklaunch/package/contents/ui/IconItem.qml
       applets/quicklaunch/package/contents/ui/layout.js
       applets/quicklaunch/package/contents/ui/main.qml
       applets/quicklaunch/package/contents/ui/Popup.qml
       applets/quicklaunch/package/contents/ui/UrlModel.qml
       applets/quicklaunch/plugin/quicklaunch_p.cpp
       applets/quicklaunch/plugin/quicklaunch_p.h
       applets/quicklaunch/plugin/quicklaunchplugin.cpp
       applets/quicklaunch/plugin/quicklaunchplugin.h
       applets/quickshare/plasmoid/contents/config/config.qml
       applets/userswitcher/package/contents/config/config.qml
       applets/userswitcher/package/contents/ui/configGeneral.qml
       applets/webbrowser/package/contents/config/config.qml
Copyright: 2014, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2013, Bhushan Shah <bhush94@gmail.com>
           2015, David Rosca <nowrep@gmail.com>
           2010-2011, Ingomar Wesp <ingomar@wesp.name>
           2014, Jeremy Whiting <jpwhiting@kde.org>
           2014-2015, Kai Uwe Broulik <kde@privat.broulik.de>
           2008-2009, Lukas Appelhans <l.appelhans@gmx.de>
           2013, Sebastian Kügler <sebas@kde.org>
           2020, Sora Steenvoort <sora@dillbox.me>
License: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

Files: applets/timer/package/contents/ui/CompactRepresentation.qml
       applets/timer/package/contents/ui/main.qml
Copyright: 2022, Fushan Wen <qydwhotmail@gmail.com>
           2016, Michael Abrahams <miabraha@gmail.com>
License: GPL-3.0-or-later

Files: applets/comic/engine/cachedprovider.cpp
       applets/comic/engine/cachedprovider.h
       applets/comic/engine/comic.cpp
       applets/comic/engine/comic.h
       applets/comic/engine/comic_package.cpp
       applets/comic/engine/comic_package.h
       applets/comic/engine/comicprovider.cpp
       applets/comic/engine/comicprovider.h
       applets/comic/engine/comicproviderkross.cpp
       applets/comic/engine/comicproviderkross.h
       applets/comic/engine/comicproviderwrapper.cpp
       applets/comic/engine/comicproviderwrapper.h
       dict/dictengine.cpp
       dict/dictengine.h
       runners/datetime/datetimerunner.cpp
       runners/datetime/datetimerunner.h
       runners/spellchecker/spellcheck.cpp
       runners/spellchecker/spellcheck.h
Copyright: 2006-2010, Aaron Seigo <aseigo@kde.org>
           2022, Alexander Lohnau <alexander.lohnau@gmx.de>
           2007, Jeff Cooper <weirdsox11@gmail.com>
           2010, Marco Martin <notmart@gmail.com>
           2010, Matthias Fuchs <mat69@gmx.net>
           2008, Petri Damstén <damu@iki.fi>
           2007, Ryan Bitanga <ephebiphobic@gmail.com>
           2007, Ryan P. Bitanga <ephebiphobic@gmail.com>
           2007, Thomas Georgiou <TAGeorgiou@gmail.com>
           2007, Tobias Koenig <tokoe@kde.org>
           2015, Vishesh Handa <vhanda@kde.org>
License: LGPL-2.0-only

Files: applets/comic/engine/types.h
       applets/grouping/container/package/contents/ui/main.qml
       applets/grouping/package/contents/ui/items/AbstractItem.qml
       applets/grouping/package/contents/ui/items/PlasmoidItem.qml
       applets/grouping/package/contents/ui/main.qml
       applets/notes/package/contents/ui/ShortcutMenuItem.qml
       applets/quickshare/plugin/contenttracker.cpp
       applets/quickshare/plugin/contenttracker.h
       runners/katesessions/katesessions.cpp
       runners/katesessions/katesessions.h
       runners/konsoleprofiles/konsoleprofiles.cpp
       runners/konsoleprofiles/konsoleprofiles.h
       wallpapers/haenau/contents/ui/BackgroundElement.qml
       wallpapers/haenau/contents/ui/BottomBackgroundElement.qml
       wallpapers/haenau/contents/ui/main.qml
       wallpapers/haenau/contents/ui/RightBackgroundElement.qml
       wallpapers/hunyango/contents/ui/main.qml
       wallpapers/potd/package/contents/ui/ActionContextMenu.qml
       wallpapers/potd/package/contents/ui/config.qml
       wallpapers/potd/package/contents/ui/main.qml
Copyright: 2011, Aaron Seigo <aseigo@kde.org>
           2020-2022, Alexander Lohnau <alexander.lohnau@gmx.de>
           2016, David Edmundson <davidedmundson@kde.org>
           2022, Fushan Wen <qydwhotmail@gmail.com>
           2017, Kai Uwe Broulik <kde@privat.broulik.de>
           2019, Luca Carlon <carlon.luca@gmail.com>
           2011-2016, Marco Martin <mart@kde.org>
           2008, Montel Laurent <montel@kde.org>
           2008, Sebastian Kügler <sebas@kde.org>
           2016, Weng Xuetian <wengxt@gmail.com>
License: LGPL-2.0-or-later

Files: po/ca/plasma_applet_org.kde.plasma.binaryclock.po
       po/ca/plasma_applet_org.kde.plasma.comic.po
       po/ca/plasma_applet_org.kde.plasma.fifteenpuzzle.po
       po/ca/plasma_applet_org.kde.plasma.fuzzyclock.po
       po/ca/plasma_applet_org.kde.plasma.notes.po
       po/ca/plasma_applet_org.kde.plasma.quickshare.po
       po/ca/plasma_applet_org.kde.plasma.timer.po
       po/ca/plasma_packagestructure_comic.po
       po/ca/plasma_runner_converterrunner.po
       po/ca/plasma_runner_katesessions.po
       po/ca/plasma_runner_spellcheckrunner.po
       po/ca@valencia/plasma_applet_org.kde.plasma.binaryclock.po
       po/ca@valencia/plasma_applet_org.kde.plasma.comic.po
       po/ca@valencia/plasma_applet_org.kde.plasma.fifteenpuzzle.po
       po/ca@valencia/plasma_applet_org.kde.plasma.fuzzyclock.po
       po/ca@valencia/plasma_applet_org.kde.plasma.notes.po
       po/ca@valencia/plasma_applet_org.kde.plasma.quickshare.po
       po/ca@valencia/plasma_applet_org.kde.plasma.timer.po
       po/ca@valencia/plasma_packagestructure_comic.po
       po/ca@valencia/plasma_runner_converterrunner.po
       po/ca@valencia/plasma_runner_katesessions.po
       po/ca@valencia/plasma_runner_spellcheckrunner.po
Copyright: 2008-2021, This_file_is_part_of_KDE
License: LGPL-2.0-or-later

Files: runners/characters/charrunner_config.cpp
       runners/characters/charrunner_config.h
       runners/characters/charrunner.cpp
       runners/characters/charrunner.h
       runners/characters/config_keys.h
       runners/converter/autotests/converterrunnertest.cpp
       runners/spellchecker/autotests/spellcheckrunnertest.cpp
Copyright: 2020, Alexander Lohnau <alexander.lohnau@gmx.de>
           2010, Anton Kreuzkamp <akreuzkamp@web.de>
           2022, Fushan Wen <qydwhotmail@gmail.com>
License: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

Files: po/ca/plasma_applet_org.kde.plasma_applet_dict.po
       po/ca/plasma_applet_org.kde.plasma.calculator.po
       po/ca/plasma_applet_org.kde.plasma.colorpicker.po
       po/ca/plasma_applet_org.kde.plasma.diskquota.po
       po/ca/plasma_applet_org.kde.plasma.keyboardindicator.po
       po/ca/plasma_applet_org.kde.plasma.konsoleprofiles.po
       po/ca/plasma_applet_org.kde.plasma.mediaframe.po
       po/ca/plasma_applet_org.kde.plasma.nightcolorcontrol.po
       po/ca/plasma_applet_org.kde.plasma.private.grouping.po
       po/ca/plasma_applet_org.kde.plasma.quicklaunch.po
       po/ca/plasma_applet_org.kde.plasma.userswitcher.po
       po/ca/plasma_applet_org.kde.plasma.weather.po
       po/ca/plasma_calendar_astronomicalevents.po
       po/ca/plasma_runner_CharacterRunner.po
       po/ca/plasma_runner_datetime.po
       po/ca/plasma_runner_konsoleprofiles.po
       po/ca/plasma_runner_krunner_dictionary.po
       po/ca/plasma_wallpaper_org.kde.potd.po
       po/ca@valencia/plasma_applet_org.kde.plasma_applet_dict.po
       po/ca@valencia/plasma_applet_org.kde.plasma.calculator.po
       po/ca@valencia/plasma_applet_org.kde.plasma.colorpicker.po
       po/ca@valencia/plasma_applet_org.kde.plasma.diskquota.po
       po/ca@valencia/plasma_applet_org.kde.plasma.keyboardindicator.po
       po/ca@valencia/plasma_applet_org.kde.plasma.konsoleprofiles.po
       po/ca@valencia/plasma_applet_org.kde.plasma.mediaframe.po
       po/ca@valencia/plasma_applet_org.kde.plasma.nightcolorcontrol.po
       po/ca@valencia/plasma_applet_org.kde.plasma.private.grouping.po
       po/ca@valencia/plasma_applet_org.kde.plasma.quicklaunch.po
       po/ca@valencia/plasma_applet_org.kde.plasma.userswitcher.po
       po/ca@valencia/plasma_applet_org.kde.plasma.weather.po
       po/ca@valencia/plasma_calendar_astronomicalevents.po
       po/ca@valencia/plasma_runner_CharacterRunner.po
       po/ca@valencia/plasma_runner_datetime.po
       po/ca@valencia/plasma_runner_konsoleprofiles.po
       po/ca@valencia/plasma_runner_krunner_dictionary.po
       po/ca@valencia/plasma_wallpaper_org.kde.potd.po
       po/uk/*
Copyright: 2008-2022, This_file_is_part_of_KDE
License: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

Files: applets/dict/plugin/dictionariesmodel.cpp
       applets/dict/plugin/dictionariesmodel.h
       applets/dict/plugin/dict_object.cpp
       applets/dict/plugin/dict_object.h
       applets/dict/plugin/dict_plugin.cpp
       applets/dict/plugin/dict_plugin.h
       applets/diskquota/package/contents/ui/ListDelegateItem.qml
       applets/diskquota/package/contents/ui/main.qml
       applets/diskquota/plugin/DiskQuota.cpp
       applets/diskquota/plugin/DiskQuota.h
       applets/diskquota/plugin/plugin.cpp
       applets/diskquota/plugin/plugin.h
       applets/diskquota/plugin/QuotaItem.cpp
       applets/diskquota/plugin/QuotaItem.h
       applets/diskquota/plugin/QuotaListModel.cpp
       applets/diskquota/plugin/QuotaListModel.h
       applets/notes/plugin/abstractnoteloader.cpp
       applets/notes/plugin/abstractnoteloader.h
       applets/notes/plugin/filesystemnoteloader.cpp
       applets/notes/plugin/filesystemnoteloader.h
       applets/notes/plugin/note.cpp
       applets/notes/plugin/note.h
       applets/notes/plugin/notemanager.cpp
       applets/notes/plugin/notemanager.h
Copyright: 2014, David Edmundson <david@davidedmundson.co.uk>
           2017, David Faure <faure@kde.org>
           2015, Dominik Haumann <dhaumann@kde.org>
License: LGPL-2.1-or-later

Files: wallpapers/potd/plugins/providers/simonstalenhagprovider.cpp
       wallpapers/potd/plugins/providers/simonstalenhagprovider.h
Copyright: 2021, Alexey Andreyev <aa13q@ya.ru>
License: LicenseRef-KDE-Accepted-GPL

Files: debian/*
Copyright: 2007-2022, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2022, Patrick Franz <deltaone@debian.org>
License: GPL-2.0-or-later

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: GPL-2.0-only
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: GPL-2.0-or-later
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA. Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in
 `/usr/share/common-licenses/GPL-2’.

License: GPL-3.0-only
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GPL License as published by the Free
 Software Foundation, version 3.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in
 `/usr/share/common-licenses/GPL-3’.

License: GPL-3.0-or-later
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in
 `/usr/share/common-licenses/GPL-3’.

License: LGPL-2.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version
 2 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.0-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option)  any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in
 `/usr/share/common-licenses/LGPL-2’.

License: LGPL-2.1-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 2.1 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.

License: LGPL-2.1-or-later
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA Also add information on how to contact you by
 electronic and paper mail.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 `/usr/share/common-licenses/LGPL-2.1’.

License: LGPL-3.0-only
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License version
 3 as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 3 can be found in
 `/usr/share/common-licenses/LGPL-3’.

License: LicenseRef-KDE-Accepted-GPL
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the license or (at
 your option) at any later version that is accepted by the membership
 of KDE e.V. (or its successor approved by the membership of KDE
 e.V.), which shall act as a proxy as defined in Section 14 of
 version 3 of the license.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

License: LicenseRef-KDE-Accepted-LGPL
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 license or (at your option) any later version that is accepted by
 the membership of KDE e.V. (or its successor approved by the
 membership of KDE e.V.), which shall act as a proxy as defined in
 Section 6 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

